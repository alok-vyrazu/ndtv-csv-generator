import requests
from bs4 import BeautifulSoup
import csv

# Make a request to the website
page = requests.get("https://www.ndtv.com/")
soup = BeautifulSoup(page.content, 'html.parser')

# Find all the news headlines and their links
news_list = soup.find_all('a', class_='item-title')

csv_list = list()

for link in news_list:
    title = link.text
    link = link.get("href")
    if title and link:
        csv_list.append({"title": title, "link": link})

# Create a CSV file and write the news headlines and links to it
with open('news.csv', mode='w', encoding='utf-8', newline='') as csv_file:
    fieldnames = ['Title', 'Link']
    writer = csv.DictWriter(csv_file, fieldnames=fieldnames)
    writer.writeheader()
    for news in csv_list:
        title = news["title"]
        link = news["link"]
        writer.writerow({'Title': title, 'Link': link})

print('Scraping complete!')
